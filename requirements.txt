flask==1.1.2
python-dotenv==0.14.0
flask-sqlalchemy==2.4.4
psycopg2==2.8.5
pika==1.1.0
dataclasses==0.6