# -*- coding: utf-8 -*-
import os
import pika
import csv
import ast
from json import dumps
from io import StringIO
from dotenv import load_dotenv
from flask import Blueprint, jsonify, request, make_response

from workflow import db, app
from workflow.models import Workflow

load_dotenv()

workflows_resource = Blueprint('workflows', __name__)

url = os.environ.get('CLOUDAMQP_URL', os.getenv('AMQP_URI'))
params = pika.URLParameters(url)
params.socket_timeout = 5
connection = pika.BlockingConnection(params) 
channel = connection.channel() 
channel.queue_declare(queue='workflows')

@workflows_resource.route('', methods=['GET'])
def get():
    workflows = Workflow.query.all()
    return jsonify([w.serialize() for w in workflows])


def on_message(channel, method_frame, header_frame, body):
    message = ast.literal_eval(body.decode('utf-8'))

    workflow = Workflow.query.get(message['uuid'])

    workflow_serialized = workflow.serialize()
    if workflow_serialized['status'] == 'inserted':
        channel.basic_ack(delivery_tag = method_frame.delivery_tag)

        workflow.status = 'consumed'
        db.session.commit()

        si = StringIO()
        csv_list = [
            [workflow_serialized['uuid'], workflow_serialized['data']]
        ]

        with open('workflows.csv', 'a', newline='') as file:
            writer = csv.writer(file)
            writer.writerows(csv_list)
    else:
        channel.basic_reject(method_frame.delivery_tag, False)
        


@workflows_resource.route('/consume', methods=['GET'])
def consume():
    channel.basic_consume(queue="workflows", on_message_callback=on_message)
    channel.start_consuming()


@workflows_resource.route('', methods=['POST'])
def post():
    data = request.get_json()
    workflow = Workflow(data['uuid'], data['data'], data['steps'])
    
    db.session.add(workflow)
    db.session.commit() 

    channel.basic_publish(exchange='', routing_key='workflows', body=dumps(workflow.serialize()))

    return workflow.serialize()

@workflows_resource.route('/<uuid>', methods=['PATCH'])
def path(uuid):
    data = request.get_json()
    
    workflow = Workflow.query.get(uuid)
    workflow.status = data['status']
    db.session.commit()

    if data['status'] == 'inserted':    
        channel.basic_publish(exchange='', routing_key='workflows', body=dumps(workflow.serialize()))

    return ('', 200)