# from dataclasses import dataclass
from enum import Enum
from json import dumps
from workflow import db
from dataclasses import dataclass
from sqlalchemy.dialects.postgresql import JSON, ARRAY, UUID

@dataclass
class Workflow(db.Model):
    __tablename__ = 'workflows'

    uuid = db.Column(UUID(as_uuid=True), primary_key=True)
    status = db.Column(db.Enum('inserted', 'consumed', name="status"))
    data = db.Column(JSON)
    steps = db.Column(ARRAY(db.String))

    def __init__(self, uuid, data, steps):
        self.uuid = uuid
        self.status = 'inserted' 
        self.data = data
        self.steps = steps

    def serialize(self):
        return {
            'uuid': str(self.uuid), 
            'status':self.status, 
            'data': self.data, 
            'steps':self.steps
            }